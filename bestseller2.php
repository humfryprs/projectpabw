<!DOCTYPE html>
<html>

<head>
  <title>Web Makeup</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url(); ?>assets/style/css/cyra-style.css" rel="stylesheet" type="text/css">
  <script src='https://kit.fontawesome.com/a076d05399.js'></script>
</head>

<body>
  <header>
    <h1 class="logo">
      CyraBeauty
    </h1>
    <div class="container">
      <div class="icon">
        <input type="search" placeholder="Search">
        <input type="submit" value="Go">
        <a href="<?php echo base_url('logreg/login'); ?>">
          <i class='far fa-user-circle' style='font-size:28px; margin-left:20px;'></i>
        </a>
      </div>
      <ul class="nav">
        <li role="presentation"><a href="<?php echo base_url('home/beranda'); ?>">HOME</a></li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="<?php echo base_url('home/makeup'); ?>">MAKE UP</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="<?php echo base_url('home/makeup'); ?>">Foundation</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/makeup'); ?>">Lipstick</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/makeup'); ?>">Blush</a></li>
          </ul>
        </li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="<?php echo base_url('home/skincare'); ?>">SKIN CARE</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="<?php echo base_url('home/skincare'); ?>">Toner</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/skincare'); ?>">Serum</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/skincare'); ?>">Mask</a></li>
          </ul>
        </li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="<?php echo base_url('home/haircare'); ?>">HAIR CARE</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="<?php echo base_url('home/haircare'); ?>">Shampoo</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/haircare'); ?>">Conditioner</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/haircare'); ?>">Hair Oil</a></li>
          </ul>
        </li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="<?php echo base_url('home/fragrance'); ?>">FRAGRANCE</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="<?php echo base_url('home/fragrance'); ?>">Eau De Toilette</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/fragrance'); ?>">Eau De Parfum</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </header>


  <div class="modal" id="popup">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-body">
          <p>You must login first! <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button> </p>
          <a class="btn btn-primary" href="login.html" role="button">Login</a>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <section class="row">
      <div class="col-4">
        <img src="<?php echo base_url(); ?>/assets/img/fss.jpg" height="339.5px" width="auto">
      </div>
      <div class="col-8">
        <h3>For Skin Sake</h3>
        <p>Gem Roller by FSS</p>
        <h2>Rp 380.000</h2>
        <p>Shade</p>
        <form>
          <div class="form-group">
            <select style="width:auto;" class="form-control btn btn-secondary dropdown-toggle" id="exampleFormControlSelect1">
              <option>Rose Quartz</option>
              <option>Jade</option>
            </select>
          </div>
        </form>
        <p>Quantity</p>
        <div class="dropdown">
          <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            1
          </button>
          <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <a class="dropdown-item" href="#">1</a>
            <a class="dropdown-item" href="#">2</a>
            <a class="dropdown-item" href="#">3</a>
          </div>
        </div>
        <br>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#popup">Add to Bag</button>
        <br>
        <div class="description">
          <h5><b>Description</b></h5>
          <p>Gem roller has been used in centuries for the healing properties.
            Facial rolling with gemstone is relaxing and yet known to be beneficial
            to your beauty ritual.
            It helps to firm skin by reduce tension in the facial muscle through
            gentle massaging
            and helps to drain the stagnation in lymphatic nodes to aid the body
            detoxification system.
            <br> Available in 2 types : Jade and Rose Quartz.</p>
        </div>
      </div>
    </section>
    <section>
      <hr>
      <h3> More Products
        <button type="button" class="btn btn-light kanan">See All ></button>
      </h3>
      <div class="row">
        <div class="col-xs-6 col-md-2">
          <a href="#" class="thumbnail">
            <img src="<?php echo base_url(); ?>/assets/img/toner1.png">
            <div class="caption">
              <h5><b>Some By Mi</b></h5>
              <p>AHA BHA Miracle</p>
              <h5><b>Rp 350.000</b></h5>
            </div>
          </a>
        </div>
        <div class="col-xs-6 col-md-2">
          <a href="#" class="thumbnail">
            <img src="<?php echo base_url(); ?>/assets/img/found3.jpg">
            <div class="caption">
              <h5><b>Make Over</b></h5>
              <p>Light Beige</p>
              <h5><b>Rp 490.000</b></h5>
            </div>
          </a>
        </div>
        <div class="col-xs-6 col-md-2">
          <a href="#" class="thumbnail">
            <img src="<?php echo base_url(); ?>/assets/img/lipstick4.jpg">
            <div class="caption">
              <h5><b>Maybelline</b></h5>
              <p>Artist Lipshot</p>
              <h5><b>Rp 250.000</b></h5>
            </div>
          </a>
        </div>
        <div class="col-xs-6 col-md-2">
          <a href="#" class="thumbnail">
            <img src="<?php echo base_url(); ?>/assets/img/blush7.jpg">
            <div class="caption">
              <h5><b>Benefit</b></h5>
              <p>Hoola Beauty</p>
              <h5><b>Rp 450.000</b></h5>
            </div>
          </a>
        </div>
        <div class="col-xs-6 col-md-2">
          <a href="#" class="thumbnail">
            <img src="<?php echo base_url(); ?>/assets/img/sampo2.jpg">
            <div class="caption">
              <h5><b>L'oreal</b></h5>
              <p>Anti Hair Fall</p>
              <h5><b>Rp 40.000</b></h5>
            </div>
          </a>
        </div>
        <div class="col-xs-6 col-md-2">
          <a href="#" class="thumbnail">
            <img src="<?php echo base_url(); ?>/assets/img/oil5.jpg">
            <div class="caption">
              <h5><b>Sukin</b></h5>
              <p>Revitalizing Oil</p>
              <h5><b>Rp 320.000</b></h5>
            </div>
          </a>
        </div>
      </div>
    </section>
  </div>

  <footer>
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-3">
          <a href="<?php echo base_url('home/aboutus'); ?>">ABOUT US</a>
        </div>
        <div class="col-3">
          <a href="#">CONTACT US</a>
        </div>
        <div class="col-3">
          <a href="#">TERMS & CONDITION</a>
        </div>
        <div class="col-3">
          <a href="#">PRIVACY POLICY</a>
        </div>
      </div>
    </div>
  </footer>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>