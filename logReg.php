<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class logReg extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
  }

  // LOGIN
  public function login()
  {
    $this->load->view('cyaraBeauty/login');
  }

  function login_user()
  {
    $this->load->model('Cyara_model');
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $where = array(
      'PASSWORD' => $password,
      'EMAIL' => $email
    );
    $cek = $this->Cyara_model->cek_login('USER', $where)->num_rows();
    if ($cek > 0) {

      $data_session = array(
        'nama' => $email,
        'status' => "login"
      );

      $this->session->set_userdata($data_session);

      redirect('home/beranda');
    } else {
      echo $this->session->set_flashdata('msg','Username or Password is Wrong');
      redirect('logreg/login');
    }
  }

  function logout()
  {
    $this->session->sess_destroy();
    redirect('home/beranda');
  }

  // REGISTER
  public function register()
  {
    $this->load->view('cyaraBeauty/register');
  }

  public function register_user()
  {
    $email = $this->input->post('email');
    $password = $this->input->post('password');
    $nama = $this->input->post('nama');
    $alamat = $this->input->post('alamat');
    $noTelp = $this->input->post('noTelp');
    $jenisKelamin = $this->input->post('jenisKelamin');

    $this->load->model('Cyara_model');
    $this->Cyara_model->insert_user($email, $password, $nama, $alamat, $noTelp, $jenisKelamin);
    redirect('logreg/login');
  }
}
