<?php
class kategori_ctrl extends CI_Controller {
  public function index() {
    $this->load->helper('url');
    $this->load->model('kategori_model');
    $posts = $this->kategori_model->get_kategori();
    $data['posts'] = $posts;
    $this->load->view('kategori', $data);
  }
  public function show1($id) {
      $this->load->helper('url');
      $this->load->model('kategori_model');
      $kategori = $this->kategori_model->get_particular_kategori($id);
      $data['posts'] = $kategori;
      if($id == 1){
        $foundie = $this->kategori_model->tampilProduk(1);
        $hasil['hasil'] = $foundie;
        $this->load->view('kategoriFound1',$hasil);
      } else if ($id ==2){
        $foundie = $this->kategori_model->tampilProduk(2);
        $hasil['hasil'] = $foundie;
        $this->load->view('kategoriFound2',$hasil);  
      } else if ($id == 3){
        $foundie = $this->kategori_model->tampilProduk(3);
        $hasil['hasil'] = $foundie;
        $this->load->view('kategoriFound3',$hasil);
      }
  }
  public function show2($id) {
    $this->load->helper('url');
    $this->load->model('kategori_model');
    $kategori = $this->kategori_model->get_particular_kategori($id);
    $data['posts'] = $kategori;
    if($id == 4){
      $lip = $this->kategori_model->tampilProduk(4);
      $hasil['hasil'] = $lip;
      $this->load->view('kategoriFound4',$hasil);
    } else if ($id ==5){
      $lip = $this->kategori_model->tampilProduk(5);
      $hasil['hasil'] = $lip;
      $this->load->view('kategoriFound5',$hasil);  
    } else if ($id == 6){
      $lip = $this->kategori_model->tampilProduk(6);
      $hasil['hasil'] = $lip;
      $this->load->view('kategoriFound6',$hasil);
    }
}
public function show3($id) {
  $this->load->helper('url');
  $this->load->model('kategori_model');
  $kategori = $this->kategori_model->get_particular_kategori($id);
  $data['posts'] = $kategori;
  if($id == 7){
    $hair = $this->kategori_model->tampilProduk(7);
    $hasil['hasil'] = $hair;
    $this->load->view('kategoriFound7',$hasil);
  } else if ($id ==8){
    $hair = $this->kategori_model->tampilProduk(8);
    $hasil['hasil'] = $hair;
    $this->load->view('kategoriFound8',$hasil);  
  } else if ($id == 9){
    $hair = $this->kategori_model->tampilProduk(9);
    $hasil['hasil'] = $hair;
    $this->load->view('kategoriFound9',$hasil);
  }
}
public function show4($id) {
  $this->load->helper('url');
  $this->load->model('kategori_model');
  $kategori = $this->kategori_model->get_particular_kategori($id);
  $data['posts'] = $kategori;
  if($id == 10){
    $fragrance = $this->kategori_model->tampilProduk(10);
    $hasil['hasil'] = $fragrance;
    $this->load->view('kategoriFound10',$hasil);
  } else if ($id ==11){
    $fragrance = $this->kategori_model->tampilProduk(11);
    $hasil['hasil'] = $fragrance;
    $this->load->view('kategoriFound11',$hasil);  
  }
}
}
