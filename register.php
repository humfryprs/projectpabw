<!DOCTYPE html>
<html>

<head>
  <title>Web Makeup</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet" type="text/css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href="<?php echo base_url(); ?>assets/style/css/cyra-style.css" rel="stylesheet" type="text/css">
  <script src='https://kit.fontawesome.com/a076d05399.js'></script>
</head>

<body>
  <header>
    <h1 class="logo">
      CyraBeauty
    </h1>
    <div class="container">
      <div class="icon">
        <input type="search" placeholder="Search">
        <input type="submit" value="Go">
        <a href="<?php echo base_url('logreg/login'); ?>">
          <i class='far fa-user-circle' style='font-size:28px; margin-left:20px;'></i>
        </a>
      </div>
      <ul class="nav">
        <li role="presentation"><a href="<?php echo base_url('home/beranda'); ?>">HOME</a></li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="cyra2-makeup.html">MAKE UP</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="cyra2-makeup.html">Foundation</a></li>
            <li><a class="dropdown-item" href="cyra2-makeup.html">Lipstick</a></li>
            <li><a class="dropdown-item" href="cyra2-makeup.html">Blush</a></li>
          </ul>
        </li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="cyra3-skincare.html">SKIN CARE</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="cyra3-skincare.html">Toner</a></li>
            <li><a class="dropdown-item" href="cyra3-skincare.html">Serum</a></li>
            <li><a class="dropdown-item" href="cyra3-skincare.html">Mask</a></li>
          </ul>
        </li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="cyra4-haircare.html">HAIR CARE</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="cyra4-haircare.html">Shampoo</a></li>
            <li><a class="dropdown-item" href="cyra4-haircare.html">Conditioner</a></li>
            <li><a class="dropdown-item" href="cyra4-haircare.html">Hair Oil</a></li>
          </ul>
        </li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="cyra5-fragrance.html">FRAGRANCE</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="cyra5-fragrance.html">Eau De Toilette</a></li>
            <li><a class="dropdown-item" href="cyra5-fragrance.html">Eau De Parfum</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </header>
  <main class="login">
    <div class='container'>
      <center>
        <form action="<?php echo base_url() . 'logreg/register_user'; ?>" method="post">
          <label for="inputEmail">Email</label>
          <input name="email" type="email" class="form-control" id="inputEmail" placeholder="Email">
          <label for="inputPassword">Password</label>
          <input name="password" type="password" class="form-control" id="inputPassword" placeholder="Password">
          <label for="inputNama">Nama</label>
          <input name="nama" type="text" class="form-control" id="inputNama" placeholder="Nama">
          <label for="inputAlamat">Alamat</label>
          <input name="alamat" type="text" class="form-control" id="inputAlamat" placeholder="Alamat">
          <label for="inputNoTelp">No Telepon</label>
          <input name="noTelp" type="text" class="form-control" id="inputNoTelp" placeholder="No Telepon">
          <label for="inputJenisKelamin">Jenis Kelamin</label>
          <input name="jenisKelamin" type="text" class="form-control" id="jenisKelamin" placeholder="Jenis Kelamin">
          <br>
          <input type="submit" value="Register" class="btn btn-primary">
        </form>
      </center>
    </div>
  </main>
  <footer>
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-3">
          <a href="aboutus.html">ABOUT US</a>
        </div>
        <div class="col-3">
          <a href="#">CONTACT US</a>
        </div>
        <div class="col-3">
          <a href="#">TERMS & CONDITION</a>
        </div>
        <div class="col-3">
          <a href="#">PRIVACY POLICY</a>
        </div>
      </div>
    </div>
  </footer>
  <script src=" https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src=" https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src=" https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>