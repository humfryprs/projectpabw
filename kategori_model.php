<?php
class kategori_model extends CI_Model {
  public function get_kategori() {
    $this->load->database();
    $query = $this->db->get('KATEGORI');
    return $query->result();
  }
  public function get_particular_kategori($id) {
      $this->load->database();
      $this->db->where("ID_KATEGORI",$id);
      $query = $this->db->get('KATEGORI');
      return $query->result();
  }
  public function tampilProduk($id){
      $query = $this->db->from('PRODUK')->join('KATEGORI','KATEGORI.ID_KATEGORI=PRODUK.ID_KATEGORI')
      ->join('BRAND','BRAND.ID_BRAND=PRODUK.ID_BRAND');
      $query->where("PRODUK.ID_KATEGORI",$id);
      return $query->get()->result();
  }
}
