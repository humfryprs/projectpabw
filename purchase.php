<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class purchase extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->helper('url');
  }

  // Barang1
  public function barang1()
  {
    $this->load->view('cyaraBeauty/bestseller1');
  }

  //Barang2
  public function barang2()
  {
    $this->load->view('cyaraBeauty/bestseller2');
  }

  //Barang2
  public function barang3()
  {
    $this->load->view('cyaraBeauty/bestseller3');
  }

  //Konfirmasi
  public function konfirmasi()
  {
    $this->load->view('cyaraBeauty/konfirmasi');
  }
}
