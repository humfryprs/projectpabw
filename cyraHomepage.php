<!DOCTYPE html>
<html>

<head>
    <title>Web Makeup</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
    <link href="<?php echo base_url(); ?>assets/style/css/cyra-style.css" rel="stylesheet" type="text/css">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
</head>

<body>
    <header>
        <h1 class="logo">
            CyraBeauty
        </h1>
        <div class="container">
            <div class="icon">
                <input type="search" placeholder="Search">
                <input type="submit" value="Go">
                <a href="<?php echo base_url('logreg/login'); ?>">
                    <i class='far fa-user-circle' style='font-size:28px; margin-left:20px;'></i>
                </a>
            </div>
            <ul class="nav">
                <li class="active" role="presentation"><a href="<?php echo base_url('home/beranda'); ?>">HOME</a></li>
                <li role="presentation"><a data-toggle="dropdown" data-target="#" href="cyra2-makeup.html">MAKE UP</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="cyra2-makeup.html">Foundation</a></li>
                        <li><a class="dropdown-item" href="cyra2-makeup.html">Lipstick</a></li>
                        <li><a class="dropdown-item" href="cyra2-makeup.html">Blush</a></li>
                    </ul>
                </li>
                <li role="presentation"><a data-toggle="dropdown" data-target="#" href="cyra3-skincare.html">SKIN CARE</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="cyra3-skincare.html">Toner</a></li>
                        <li><a class="dropdown-item" href="cyra3-skincare.html">Serum</a></li>
                        <li><a class="dropdown-item" href="cyra3-skincare.html">Mask</a></li>
                    </ul>
                </li>
                <li role="presentation"><a data-toggle="dropdown" data-target="#" href="cyra4-haircare.html">HAIR CARE</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="cyra4-haircare.html">Shampoo</a></li>
                        <li><a class="dropdown-item" href="cyra4-haircare.html">Conditioner</a></li>
                        <li><a class="dropdown-item" href="cyra4-haircare.html">Hair Oil</a></li>
                    </ul>
                </li>
                <li role="presentation"><a data-toggle="dropdown" data-target="#" href="cyra5-fragrance.html">FRAGRANCE</a>
                    <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="cyra5-fragrance.html">Eau De Toilette</a></li>
                        <li><a class="dropdown-item" href="cyra5-fragrance.html">Eau De Parfum</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </header>

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="margin-bottom: 30px;">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="<?php echo base_url(); ?>assets/img/wallpaper.jpeg" class="carousel-img" alt="hello!">
                <div class="carousel-caption">
                    <h1 class="caption">BEST SKINCARE THAT YOU SHOULD HAVE
                        <br> <br>
                        <button class="btn btn-primary btn-lg">
                            Read More
                        </button>
                    </h1>
                </div>
            </div>
            <div class="carousel-item">
                <img src="<?php echo base_url(); ?>assets/img/wallpaper2.jpeg" class="carousel-img" alt="...">
                <div class="carousel-caption">
                    <h1 class="caption">MOST FAMOUS KOREAN MAKE UP
                        <br> <br>
                        <button class="btn btn-primary btn-lg">
                            Read More
                        </button>
                    </h1>
                </div>
            </div>
            <div class="carousel-item">
                <img src="<?php echo base_url(); ?>assets/img/wallpaper3.jpg" class="carousel-img" alt="...">
                <div class="carousel-caption">
                    <h1 class="caption">TIPS & TRICKS TO ACHIEVE CLEAN BRUSH
                        <br> <br>
                        <button class="btn btn-primary btn-lg">
                            Read More
                        </button>
                    </h1>
                </div>
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
        <script>
            $('.carousel').carousel()
        </script>
    </div>

    <main>
        <div class="container">
            <section>
                <div class="heading">
                    <hr>
                    <h3>NEW PRODUCTS</h3>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-4">
                        <a href="bestseller1.html" class="thumbnail">
                            <img src="<?php echo base_url(); ?>assets/img/laneige.jpeg" class="thumbnail">
                            <div class="caption">
                                <h5>Laneige</h5>
                                <p>Lip Sleeping Mask</p>
                                <h5>Rp 250.000</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-4">
                        <a href="bestseller2.html" class="thumbnail">
                            <img src="<?php echo base_url(); ?>assets/img/fss.jpg" class="thumbnail">
                            <div class="caption">
                                <h5>For Skin Sake</h5>
                                <p>Gem Roller by FSS</p>
                                <h5>Rp 380.000</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-4">
                        <a href="bestseller3.html" class="thumbnail">
                            <img src="<?php echo base_url(); ?>assets/img/hehe.jpeg" class="thumbnail">
                            <div class="caption">
                                <h5>Glossier</h5>
                                <p>Super Glow Vitamin C Serum</p>
                                <h5>Rp 390.000</h5>
                            </div>
                        </a>
                    </div>
                </div>
            </section>
            <section>
                <div class="heading">
                    <hr>
                    <h3>BEST SELLER</h3>
                    <hr>
                </div>
                <br>
                <div class="row">
                    <div class="col-4">
                        <a href="#" class="thumbnail">
                            <img src="<?php echo base_url(); ?>assets/img/glossier.png" class="thumbnail">
                            <div class="caption">
                                <h5>LA Girl</h5>
                                <p>Mascara Gel</p>
                                <h5>Rp 250.000</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-4">
                        <a href="#">
                            <img src="<?php echo base_url(); ?>assets/img/smashbox.jpeg" class="thumbnail">
                            <div class="caption">
                                <h5>Smashbox</h5>
                                <p>Perfect Nude</p>
                                <h5>Rp 300.000</h5>
                            </div>
                        </a>
                    </div>
                    <div class="col-4">
                        <a href="#" class="">
                            <img src="<?php echo base_url(); ?>assets/img/hudaa.jpg" class="thumbnail">
                            <div class="caption">
                                <h5>Huda Beauty</h5>
                                <p>Cream Contour</p>
                                <h5>Rp 500.000</h5>
                            </div>
                        </a>
                    </div>
                </div>
            </section>
        </div>
    </main>

    <footer>
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-3">
                    <a href="aboutus.html">ABOUT US</a>
                </div>
                <div class="col-3">
                    <a href="#">CONTACT US</a>
                </div>
                <div class="col-3">
                    <a href="#">TERMS & CONDITION</a>
                </div>
                <div class="col-3">
                    <a href="#">PRIVACY POLICY</a>
                </div>
            </div>
        </div>
    </footer>
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>