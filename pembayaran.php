<!DOCTYPE html>
<html>

<head>
  <title>Web Makeup</title>
  <link href='<?php echo base_url(); ?>assets/style/css/payment.css' rel='stylesheet' type='text/css'>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
  <link href="<?php echo base_url(); ?>assets/style/css/cyra-style.css" rel="stylesheet" type="text/css">
  <script src='https://kit.fontawesome.com/a076d05399.js'></script>
</head>

<body>
  <header>
    <h1 class="logo">
      CyraBeauty
    </h1>
    <div class="container">
      <div class="icon">
        <input type="search" placeholder="Search">
        <input type="submit" value="Go">
        <a href="<?php echo base_url('logreg/login'); ?>">
          <i class='far fa-user-circle' style='font-size:28px; margin-left:20px;'></i>
        </a>
      </div>
      <ul class="nav">
        <li class="active" role="presentation"><a href="<?php echo base_url('home/beranda'); ?>">HOME</a></li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="<?php echo base_url('home/makeup'); ?>">MAKE UP</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="<?php echo base_url('home/makeup'); ?>">Foundation</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/makeup'); ?>">Lipstick</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/makeup'); ?>">Blush</a></li>
          </ul>
        </li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="<?php echo base_url('home/skincare'); ?>">SKIN CARE</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="<?php echo base_url('home/skincare'); ?>">Toner</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/skincare'); ?>">Serum</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/skincare'); ?>">Mask</a></li>
          </ul>
        </li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="<?php echo base_url('home/haircare'); ?>">HAIR CARE</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="<?php echo base_url('home/haircare'); ?>">Shampoo</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/haircare'); ?>">Conditioner</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/haircare'); ?>">Hair Oil</a></li>
          </ul>
        </li>
        <li role="presentation"><a data-toggle="dropdown" data-target="#" href="<?php echo base_url('home/fragrance'); ?>">FRAGRANCE</a>
          <ul class="dropdown-menu">
            <li><a class="dropdown-item" href="<?php echo base_url('home/fragrance'); ?>">Eau De Toilette</a></li>
            <li><a class="dropdown-item" href="<?php echo base_url('home/fragrance'); ?>">Eau De Parfum</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </header>


  <main class="container">
    <div class="size">
      <h2>
        Pembayaran
      </h2>
    </div>
    <hr>
    <div class="row justify-content-center align-content-center paymentCont">
      <div class="col-12 headingWrap">
        <h3 class="headingTop size">Pilih Metode Pembayaran</h3>
      </div>
      <div class="col-12 paymentWrap">
        <center>
          <div class="btn-group paymentBtnGroup btn-group-justified" data-toggle="buttons">
            <label class="btn paymentMethod active">
              <div class="method bca"></div>
              <input type="radio" name="options">
            </label>
            <label class="btn paymentMethod">
              <div class="method mandiri"></div>
              <input type="radio" name="options">
            </label>
            <label class="btn paymentMethod">
              <div class="method bni"></div>
              <input type="radio" name="options">
            </label>
            <label class="btn paymentMethod">
              <div class="method bri"></div>
              <input type="radio" name="options">
            </label>
            <label class="btn paymentMethod">
              <div class="method ovo"></div>
              <input type="radio" name="options">
            </label>
            <label class="btn paymentMethod">
              <div class="method gopay"></div>
              <input type="radio" name="options">
            </label>
          </div>
        </center>
        </div>
      <div class="footerNavWrap clearfix">
        <div class="btn btn-success pull-left btn-fyi">
          <span class="glyphicon glyphicon-chevron-left"> Kembali Ke
            Keranjang
          </span>
        </div>
        <div class="btn btn-success pull-right btn-fyi">
          <span class="glyphicon glyphicon-chevron-right">Checkout
          </span>
        </div>
      </div>
    </div>
  </main>
  <footer>
    <div class="container">
      <div class="row align-items-center justify-content-center">
        <div class="col-3">
          <a href="<?php echo base_url('home/aboutus'); ?>">ABOUT US</a>
        </div>
        <div class="col-3">
          <a href="#">CONTACT US</a>
        </div>
        <div class="col-3">
          <a href="#">TERMS & CONDITION</a>
        </div>
        <div class="col-3">
          <a href="#">PRIVACY POLICY</a>
        </div>
      </div>
    </div>
  </footer>
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>

</html>