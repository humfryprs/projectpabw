<?php
class Cyara_model extends CI_Model
{
  public function get_posts()
  {
    $this->load->database();
    $query = $this->db->get('post');
    return $query->result();
  }

  public function insert_user($email, $password, $nama, $alamat, $noTelp, $jenisKelamin)
  {
    $this->load->database();
    $data = array(
      'PASSWORD' => $password,
      'EMAIL' => $email,
      'NAMA' => $nama,
      'ALAMAT' => $alamat,
      'NO_TELP' => $noTelp,
      'JENIS_KELAMIN' => $jenisKelamin
    );
    $this->db->insert('USER', $data);
  }

  public function login($data)
  {
    $condition = "EMAIL =" . "'" . $data['EMAIL'] . "' AND " . "PASSWORD =" . "'" . $data['PASSWORD'] . "'";
    $this->db->select('EMAIL, PASSWORD');
    $this->db->from('user');
    $this->db->where(1);
    $this->db->limit(1);
    $query = $this->db->get();

    if ($query->num_rows() == 1) {
      return true;
    } else {
      return false;
    }
  }

  public function cek_login($table, $where)
  {
    return $this->db->get_where($table, $where);
  }
}
