<!DOCTYPE html>
<html>
<head>
    <title>Web Makeup</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
        integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link href='https://fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"
        integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
        <style id="stylesheet" type="text/css">
    .icon{
    padding-right: 1%;
    text-align : right;
    font-size: 15px;
}

#brand {
    padding-top: 2%;
}

.button {
   
    text-align : right;
    font-size: 20px;
}

.kanan {
    position: absolute;
    right: 14%;
}
.position {
    position: absolute;
    text-align: auto;
    transform: translate(-50%, -50%);
}
img{
    position : relative;
    width : 100%;
}
.caption{
    color:white;
    text-align: center;
    padding-bottom: 250px;
    left: auto;
    top: auto;
    width :auto;
}
.menu:hover .dropdown-menu{
    display: block;
}

.ukuran {
    width: 100%;
    height: 820px;
  }

.size {
    padding-left: 15%;
    padding-right: 15%;
    text-align: justify;
   border-left: 30%;
 
}

.rounded-circle{
    height: 25%;
    width:25%;
}

.about{
    padding-right: 10%;
    padding-left: 10%;
}


.col-4 {
    padding-top: 6%;
    padding-left: 3%;
    padding-right: 3%;
 }
 .bawah{
     position:relative;
     margin-top:10%;
 }
 </style>
</head>

<body>
    <div style="background-color:#f8e2e2;">
        <center>
            <div id="brand">
                <p style="font-family:serif; font-size: 50px; font-weight: bolder"> <i>CyraBeauty</i> </p>
            </div>
        </center>
        <div class="icon">
            <input type="search" placeholder="Search" style="float:right">
        </div>
        <div class="container" style="padding-bottom: 10px;">
            <ul class="nav nav-pills nav-justified">
                <li role="presentation" style="width: 19%;"><a href="cyra.html">HOME</a></li>
                <li role="presentation" class="active" style="width: 19%;"><a href="cyra2.html">MAKE UP</a></li>
                <li role="presentation" style="width: 19%;"><a href="cyra3-skincare.html">SKIN CARE</a></li>
                <li role="presentation" style="width: 19%;"><a href="cyra4-haircare.html">HAIR CARE</a></li>
                <li role="presentation" style="width: 19%;"><a href="fragrance.html">FRAGRANCE</a></li>
            </ul>
        </div>
    </div>
    <div class="container">

        <div class="row">

            <div class="col-3">
                <HR>
                <h2>MAKE UP</h2>
                <BR>
                <ul class="list-group list-group-flush">
                    <a href="seeall1.html" class="list-group-item">FOUNDATION</a>
                    <a href="seeall2.html" class="list-group-item">LIPSTICK</a>
                    <a href="seeall3.html" class="list-group-item">BLUSH</a>
                </ul>
            </div>
            <div class="col-lg-9">
                <br><br><br><br><br>  
                <div class = "row">             
                <?php foreach ($hasil as $item):?>
                
                <div class="col-lg-3">
                    <a href="#" class="thumbnail">
                    <?php 
                $this->db->select('GAMBAR');
                $this->db->where ("ID_KATEGORI = 1");
                $query = $this->db->get('PRODUK');
               
                foreach ($query->result() as $row){
                    $gambar=array('img'=>$row->GAMBAR);
                    $item->ID_PRODUK;
                } ?>
                <php print_r($gambar); die(); ?>
                <div class="gambar">
                    <?php echo "<img src=data:image/jpeg;base64,".base64_encode($gambar['img'] ).">"; ?>
                        </div>
                        <div class="caption">
                            <h5><b><?= $item->BRAND;?></b></h5>
                            <p><?= $item->NAMA_PRODUK;?></p>
                            <h5><b><?= $item->HARGA;?></b></h5>
                        </div>
                        </div>
                    </a>
                    <?php endforeach?>
                </div>
                </div>
            </div>
        </div>
        <div style="background-color:#f8e2e2;">
            <div class="container bawah" style="padding-bottom: 30px;" style="padding-top: 10px;">
                <ul class="nav nav-pills nav-justified" style="padding-top:10px">
                    <li role="presentation" style="width: 23%;"><a href="aboutus.html">ABOUT US</a></li>
                    <li role="presentation" style="width: 23%;"><a href="#">CONTACT US</a></li>
                    <li role="presentation" style="width: 23%;"><a href="#">TERMS & CONDITION</a></li>
                    <li role="presentation" style="width: 23%;"><a href="#">PRIVACY POLICY</a></li>
                </ul>
            </div>
        </div>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"
            integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd"
            crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</body>

</html>